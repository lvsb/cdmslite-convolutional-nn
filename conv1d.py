from network import * 
from keras.optimizers import Adam
from keras.utils.vis_utils import plot_model
import tensorflow as tf
import keras.backend as K
from loss import *
from callback import EarlyStoppingWithCutoff as EarlyStopping
import numpy as np
from numpy.random import choice
from pickle import dump, load
import h5py
import sys
import pandas as pd

def parse(fname,key,flatten=True, mode = '1d'):
    df = pd.read_hdf(fname,key=key,mode='r')
    #df = h5py.File(fname,mode='r')
    traces= [t.reshape([-1]) if flatten else t for t in df[key]['tm']]#.values]
    tms   = np.stack([t for t in traces]) 
    epos  = np.stack([[p[1], p[2], p[3], p[4], p[0]] for p in df[key]['epos']])#.values])
    S     = np.stack([ ([p[-1]] if mode == '1d' else [p[-1]]*11) for p in df[key]['epos']])#.values])
    return tms,epos,S

name = 'conv1d_cdmslite'

if __name__ == '__main__':
	num_trials = 200

	# scan range
	n_c_layers = [1,4]
	n_c_filter = [16,32,64,128]
	n_f_layers = np.arange(9,30)
	n_f_hidden = np.arange(5,20)   
	window = np.arange(1,10)
	stride = np.arange(1,10)
	lr = np.logspace(-4,-2,3)
	lr_decay = np.logspace(-4,-1,4)
	dropout = [0]
	b_size = [32,64,128,256]
	l_bias = [0]	
	# prepare dataset
	dataset = 'cdmslite_wimp_sim_data.h5'
	if len(sys.argv) > 1:
		dataset = sys.argv[1]
	print(dataset)
	X_train, Y_train, S_train = parse(dataset,key='train',flatten=False)
	X_dev, Y_dev, S_dev = parse(dataset,key='dev',flatten=False)
	n_input  = X_train[0].shape
	n_output = len(Y_train[0])
	
	# reshape input
	X_train,S_train = reshape_input(X_train,S_train)
	X_dev,S_dev = reshape_input(X_dev,S_dev)
	# appending unit confidence to output
	Y_train = reshape_output(Y_train)
	Y_dev = reshape_output(Y_dev)	

	data = [X_train, Y_train, S_train, X_dev, Y_dev, S_dev]
	# initialize best results
	best_resolution = 9999
	best_params = {}

	# check if there is already a saved best fit
	import os.path
	if os.path.isfile('model/%s.h5'%name):
		best_model = load_model(name)
		best_model.summary()
		best_params = load(open('model/%s.params'%name,'rb'))

		best_resolution = resolution(best_model.predict([X_dev,S_dev]),Y_dev)
		print('previously tuned model found.')
		print(best_params)
		print('previous best resolution = %.2f'%best_resolution)
		K.clear_session()

	for i in range(num_trials):	
		# generate layer structure
		layers = []
		layers.append(n_input)
		layers.append('coord1d')
		layers.append( (128,int(choice(window)), int(choice(stride)), 'conv1d') )
		for k in range(choice(n_c_layers)):
			layers.append( (int(choice(n_c_filter)), int(choice(window)), int(choice(stride)) ,'conv1d'))
		layers.append((int(choice(n_f_hidden)),'condense1d'))
		for k in range(choice(n_f_layers)):
			layers.append( (int(choice(n_c_filter)), int(choice(window)), choice(dropout), 'sepconv1d'))
		layers.append('flatten')
		layers.append(n_output)
	
		# train model
		params = { 'layers'        : layers, 
			   'lambda_bias'   : choice(l_bias),
			   'learning_rate' : choice(lr),
			   'decay_rate'    : choice(lr_decay),
			   'batch_size'    : choice(b_size) }
		print()
		print('starting trial #%d'%i)
		print(params)
		try:
			res, model, hist = fit_model(data, params)
			if res < best_resolution and res > 0:
				best_resolution = res
				best_params = params
				model.save('model/%s.h5'%name)
				model.save_weights('model/%s.weights'%name)
				plot_model(model, to_file='model_plot.svg', show_shapes=True, show_layer_names=True)
				dump(params, open('model/%s.params'%name,'wb'))
				dump(hist, open('model/%s.hist'%name,'wb'))
			K.clear_session()
		except KeyboardInterrupt:
			K.clear_session()
			print()
			reply = str(input('Exit training? (y/N): ')).lower().strip()
			if reply == 'y':
				break
			else:
				continue
		except:
			K.clear_session()
			continue
		
		print(params)
		print('this resolution = %.2f'%res)
		print('------------------------------------------')
		print(best_params)
		print('best resolution = %.2f'%best_resolution)

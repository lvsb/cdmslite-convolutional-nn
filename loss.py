from keras import backend as K
from keras.losses import binary_crossentropy as xentropy
from keras.losses import mean_squared_error as mse
import tensorflow as tf
import numpy as np
from matplotlib.pyplot import hist
from scipy.optimize import curve_fit

def loss(y_true, y_pred):
	recon_mode = K.cast(K.equal(K.max(y_true[:,-1]), K.min(y_true[:,-1])),dtype='float32')
	conf_loss = xentropy(y_true[:,-1],y_pred[:,-1]) 

	R_loss = mse(y_true[:,0],y_pred[:,0])
	theta_loss = mse(y_true[:,1],y_pred[:,1])
	Z_loss = mse(y_true[:,2],y_pred[:,2])
	E_loss = mse(y_true[:,4],y_pred[:,4])
	t_loss = xentropy(y_true[:,3],y_pred[:,3])
	R_loss2 = xentropy(y_true[:,0],y_pred[:,0])
	recon_loss = theta_loss+Z_loss+R_loss+t_loss+E_loss*10

	return recon_mode * recon_loss + (1-recon_mode)*conf_loss

def fit_sigma(data):
	def gaussian(x, amp, sig):
		return (amp / (np.sqrt(2*np.pi) * sig)) * np.exp(-(x)**2 / (2*sig**2))
	init_vals = [1, 10]  # for [amp,sigma] 
	bins = np.linspace(min(data),max(data),100)
	n,_,_ = hist(data,bins=bins,normed=1)
	try:
		best_vals, covar = curve_fit(gaussian, bins[:-1], n, p0=init_vals)
		if best_vals[1]<0:
			return 9999
		else:
			return best_vals[1]
	except:
		return 9999

def pos_resolution(y_pred,y_true):
	X_true = y_true[:,0]* np.cos( y_true[:,1]*np.pi)*50
	X_pred = y_pred[:,0]* np.cos( y_pred[:,1]*np.pi)*50
	Y_true = y_true[:,0]* np.sin( y_true[:,1]*np.pi)*50
	Y_pred = y_pred[:,0]* np.sin( y_pred[:,1]*np.pi)*50
	x_res  = fit_sigma(X_true-X_pred)
	y_res  = fit_sigma(Y_true-Y_pred)
	z_res  = fit_sigma((y_true-y_pred)[:,2])*16.7
	return (x_res+y_res+z_res)/3.

def energy_resolution(y_pred,y_true):
	E_true = np.exp(y_true[:,4]*10)
	E_pred = np.exp(y_pred[:,4]*10)
	return fit_sigma(E_true-E_pred)

def resolution(y_pred,y_true):
	return (pos_resolution(y_pred,y_true)*3 + energy_resolution(y_pred,y_true))/4.

def confidence(y_pred, y_true):
	return np.exp(-100*np.sum(np.square(y_pred[:,:-1] - y_true[:,:-1]),axis=-1,keepdims=True))

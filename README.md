# CDMSlite convolutional NN

This repository includes a sample of the deep learning work undertaken by
Lucas Bezerra as part of the SuperCDMS collaboration. The goal of these neural
networks is to reconstruct the energy of simulated events in a particle detector.

network.py includes the layer structure and model specifics of the neural network,
whereas conv1d.py actually generates neural networks with randomized hyperparameters
and tests their effectiveness by comparing the resolution of energy reconstructions.